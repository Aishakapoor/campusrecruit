# Generated by Django 2.1.7 on 2019-05-02 04:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('campapp', '0004_auto_20190501_1859'),
    ]

    operations = [
        migrations.AddField(
            model_name='postj',
            name='profile_pic',
            field=models.ImageField(null=True, upload_to='images/%Y/%m/%d'),
        ),
        migrations.AlterField(
            model_name='postj',
            name='experience',
            field=models.CharField(max_length=200, null=True),
        ),
    ]
