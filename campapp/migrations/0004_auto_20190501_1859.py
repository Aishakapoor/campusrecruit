# Generated by Django 2.1.7 on 2019-05-01 13:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('campapp', '0003_postj'),
    ]

    operations = [
        migrations.AlterField(
            model_name='postj',
            name='contact',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='postj',
            name='description',
            field=models.TextField(null=True),
        ),
        migrations.AlterField(
            model_name='postj',
            name='hrname',
            field=models.CharField(max_length=30, null=True),
        ),
        migrations.AlterField(
            model_name='postj',
            name='requirements',
            field=models.TextField(null=True),
        ),
        migrations.AlterField(
            model_name='postj',
            name='salary',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
