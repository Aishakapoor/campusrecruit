from django.contrib import admin
from campapp.models import contact,company,student,postj,addQuestion,apply1
admin.site.site_header='Campus Recruitment'

admin.site.register(contact)
admin.site.register(company)
admin.site.register(student)
admin.site.register(postj)
admin.site.register(addQuestion)
admin.site.register(apply1)

# Register your models here.
